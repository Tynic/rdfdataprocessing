from rdflib.plugins.sparql import prepareQuery
from rdflib import URIRef, Literal,Namespace, XSD, term
from rdflib.graph import Graph, Resource
from flask import Flask, render_template, request, redirect, send_file
from io import BytesIO
import datetime
app = Flask(__name__)

# graph for loading triples
g = Graph()
# dictionary for storing data
d = {}
# lists for storing gui values
resource_list = []
list_prop = []
prep_pro_list = []

def get_prefix(uri):
    for pred, pred_uri in g.namespaces():
        if uri == pred_uri:
            return pred

def get_uri(pred):
    for pred_, uri in g.namespaces():
        if pred == pred_:
            return uri

def mapping(str_type):
    xsd_type = XSD.string

    if str_type == "xsd:float":
        xsd_type = XSD.float
    elif str_type == "xsd:int":
        xsd_type = XSD.int
    elif str_type == "xsd:date":
        xsd_type = XSD.date
    elif str_type == "xsd:string":
        xsd_type = XSD.string

    return xsd_type


def retype(prop, str_type):
    _ = prop.split(':')
    pred = _[0]
    name = _[1]
    uri = get_uri(pred)

    qstr = str("SELECT ?s ?p ?o WHERE { ?s " + prop + " ?o . }")
    q = prepareQuery(qstr, initNs={pred: uri})

    pred_uri = URIRef(uri + name)

    for row in g.query(q):
        if str_type.startswith('http://'):
            g.remove((row.s, pred_uri, row.o))
            tempres = Resource(g, row.s)
            tempres.add(pred_uri, URIRef(str_type + str(row.o)))
        elif check_conversion(str_type, row.o) == True:
            type = mapping(str_type)
            g.remove((row.s, pred_uri, row.o))
            g.add((row.s, pred_uri, Literal(row.o, datatype=type)))
        else:
            for rowP in g.query(q):
                g.remove((rowP.s, pred_uri, rowP.o))
                g.add((rowP.s, pred_uri, Literal(rowP.o, datatype=None)))
            return False
    return True

def check_conversion(str_type, value):
    if str_type == "xsd:int":
        try:
            int(value)
            return True
        except:
            return False
    elif str_type == "xsd:float":
        try:
            float(value)
            return True
        except:
            return False
    elif str_type == "xsd:date":
        try:
            datetime.date(value)
            return True
        except:
            return False
    elif str_type == "xsd:string":
        return True

@app.route('/', methods=['POST', 'GET'])
def my_form():
    error = None
    if request.method == 'POST':
        resource_list.clear()
        list_prop.clear()
        prep_pro_list.clear()
        url = request.form['text']
        try:
            #g.load('20171009-abridged2.ttl', format='n3')
            g.remove((None, None, None))
            g.load(url, format='n3')
            return redirect('/results.html')
        except:
            error = 'Data could not be processed. Please try again.'

    return render_template("my-form.html", error=error)

def suggest_datatype(literal):
    datatypes_list = []

    # if numeric
    if literal.isnumeric():
        datatypes_list.append("xsd:int")
        if isinstance(literal, float):
            datatypes_list.insert(0, "xsd:float")

        datatypes_list.append("xsd:float")
        datatypes_list.append("xsd:string")
        datatypes_list.append("xsd:date")

    elif isinstance(literal, datetime.date):
        datatypes_list.append("xsd:date")
        datatypes_list.append("xsd:int")
        datatypes_list.append("xsd:float")
        datatypes_list.append("xsd:string")

    else:
        datatypes_list.append("xsd:string")
        datatypes_list.append("xsd:int")
        datatypes_list.append("xsd:float")
        datatypes_list.append("xsd:date")

    datatypes_list.extend(resource_list)
    return datatypes_list

@app.route('/results.html', methods=['POST', 'GET'])
def results():
    error_msg = None

    if request.form.get('resource') != None:
        res = request.form['resource']
        print(res)
        uri = URIRef(res)
        resource = g.resource(uri)
        assert  isinstance(resource, Resource)
        assert resource.identifier is uri
        assert resource.graph is g
        resource_list.append(res)

    if request.form.get('button') != None:
        print('POST')
        prop = request.form.get('button')
        sel_type = request.form.get(prop)

        for idx, item in enumerate(list_prop):
            if item[0] == prop:
                if retype(prop, sel_type) == False:
                    list_prop[idx][2] = "None"
                    error_msg = "Conversion not valid. Property " + prop + " set as None."
                else:
                    list_prop[idx][2] = sel_type
                    error_msg = None
                break;

    if not list_prop:
        qstr = "SELECT DISTINCT ?p WHERE { ?s ?p ?o . } order by asc(str(?p)) "
        q = prepareQuery(qstr)

        for row in g.query(q):
            _ = row[0].split('/')
            prop_name = _[-1]

            uri = URIRef(row[0].split(prop_name)[0])
            pred = get_prefix(uri)

            if pred is not None:
                pref_pro = pred+":"+prop_name
            else:
                pref_pro = prop_name
            prep_pro_list.append([pref_pro, pred, uri])

    list_prop.clear()
    # check possible data types
    for pref_pro, pred, uri in prep_pro_list:
        select_example = "SELECT ?o WHERE { ?s " + pref_pro + " ?o . } LIMIT 1 "

        try:
            se = prepareQuery(select_example, initNs={pred: uri})
        except:
            continue

        for item in g.query(se):
            literal = item[0]
        # check type of literal
        list_datatype = suggest_datatype(literal)

        if type(literal) is URIRef:
            xsdtype = literal
        else:
            xsdtype = literal.datatype
        list_prop.append([pref_pro, list_datatype, xsdtype])

    # print to file
    if request.form.get('export_btn') != None:
        buffer = BytesIO()
        w = g.serialize(format='n3')
        buffer.write(w)
        buffer.seek(0)
        return send_file(buffer, as_attachment=True,
                         attachment_filename='a_file.txt',
                         mimetype='text/csv')

    y_val=None
    vals_hist=None
    labels=None
    graph = False
    hist_name = None

    # generate histogram
    if request.form.get('hist_btn') != None:
        variable = request.form.get('hist_btn')
        hist_name = variable
        _ = variable.split(':')
        pred = _[0]
        name = _[1]
        uri = get_uri(pred)
        hist_str = str("SELECT ?o WHERE { ?s " + variable + " ?o . }")
        vals_query = prepareQuery(hist_str, initNs={pred: uri})

        # sort names
        vals_list = []
        for row in g.query(vals_query):
            vals_list.append(row.o)
        vals_list.sort()

        vals_hist = []
        labels = []
        count = 0
        old_val = vals_list[0]

        for val in vals_list:
            count+=1
            if val != old_val:
                vals_hist.append(count-1)
                labels.append(str(old_val))
                count = 0
                old_val = val

        vals_hist.append(count)
        labels.append(str(val))
        y_val = max(vals_hist)
        graph = True

    count_triples = len(g)
    table_list = list_prop

    return render_template('Chart.html', property=hist_name, graph=graph, y_val=y_val, error_msg=error_msg, values=vals_hist, labels=labels, count_triples=str(count_triples), table_list=table_list)

if __name__ == '__main__':
    app.run()